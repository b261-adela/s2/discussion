package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    // Java Collection
        // are a single unit of objects
        // useful for manipulating relevant pieces of data that can be used in different situations
    public static void main(String[] args){
        // Arrays
            // are containers of values of the SAME data type given a pre-defined amount of values
            // Java arrays are more rigid, once the size and data type are defined, they can no longer be change

        // Syntax: Array Declaration
            //dataType[] identifier = new dataType[numOfElements]
            // the values of the array is initialized to 0 or null

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;

        System.out.println(intArray[2]);
        //prints memory address of the array
        System.out.println(intArray);

        //to print all values in the intArray
        System.out.println(Arrays.toString(intArray));

        boolean[] bArr = new boolean[2];
        System.out.println(Arrays.toString(bArr));

        //Declaring array with initialization
        //Syntax:
            //dataType[] identifier = {elementA, elementB,... elementNth}
            //same with??
            //dataType[] identifier = new dataType[] {elementA, elementB,... elementNth}

        String[] names = new String[] {"John", "Jane", "Joe"};
        System.out.println(Arrays.toString(names));
        System.out.println(names.length);

        // Sample Java Arrays methods
        Arrays.sort(intArray);
        System.out.println("Order of items after sort: " + Arrays.toString(intArray));

        // Multi-dimensional Arrays
        // Syntax:
            //dataType[][] identifier = new dataType [rowLength][colLength]

        // String[][] classroom = new String [][] { {0},{1},.....{nth} };
        String[][] classroom = new String [3][3];
        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Jane";
        classroom[1][2] = "Jobert";
        //Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofey";

        //prints memory address
        // System.out.println(Arrays.toString(classroom));
        // we use to the deepToString() method when printing multidiemnsional array
        System.out.println(Arrays.deepToString(classroom));

        //Arraylists
            // are resizable arrays, wherein elements can be added or removed whenever it is needed.

            // Syntax:
                //ArrayList<dataType> identifier = new ArrayList<dataType>()

            // Declare an arraylist
//        ArrayList<String> students = new ArrayList<>();
//
//        // Add elements to ArrayList is done by the add() function
//        // students.add(0,"Jolai"); can specify index of new element
//        students.add("John");
//        students.add("Paul");
//
//        // does not print memory address unlike in Arrays
//        System.out.println(students);

        //Declare an ArrayList with Values
        ArrayList<String> students = new ArrayList<>(Arrays.asList("John", "Paul"));
        System.out.println(students);

        // Accessing elements to an ArrayList is done by using get(index) method
        System.out.println(students.get(1));

        // Updating an element
        // arrayListName.set(index, element)
        students.set(1,"George");
        System.out.println("After updating: "+ students);

        // Adding an element on a specific index
        // arrayListName.add(index,element)
        students.add(1, "Mike");
        System.out.println("After adding to index 1: " + students);

        // Remove Specific element
        // arrayListName.remove(index)
        students.remove(1);
        System.out.println("After removing index 1: " + students);

        // Removing all elements
        students.clear();
        System.out.println("After removing all: " + students);

        //Getting the number of elements in an ArrayList
        System.out.println("Size of arraylist: " + students.size());

        //
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2));
        System.out.println(numbers);

        //HashMaps
            // Collection of data in "key-value pairs"
            // in Java, "keys" also referred by the "fields"
            // wherein the values are accessed by the "fields"

        // Syntax :
            // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>()

        //Declaring HashMaps

//        HashMap<String, String> jobPosition = new HashMap<>();
//        System.out.println(jobPosition);
//
//        jobPosition.put("Student", "Alice");
//        jobPosition.put("Developer", "Magic");
//        System.out.println(jobPosition);

        HashMap<String,String> jobPosition = new HashMap<>() {
            {
                put("Student", "Alice");
                put("Developer", "Magic");
            }
        };
        System.out.println(jobPosition);

        // Accessing elements in HashMaps
        System.out.println(jobPosition.get("Student"));

        //Updating an element
        jobPosition.replace("Student","Jacob");
        System.out.println("After updating student: " +jobPosition);

        // when put is used, and if key is existing, it replaces the value
        jobPosition.put("Student", "Jacob P");
        System.out.println("After updating student using put: " +jobPosition);

        System.out.println("Fields: " +jobPosition.keySet());
        System.out.println("Values: " +jobPosition.values());


        // Removing a key-value pair
        jobPosition.remove("Student");
        System.out.println("After removing student: " +jobPosition);

        // Removing all
        jobPosition.clear();
        System.out.println(jobPosition);


    }
}
