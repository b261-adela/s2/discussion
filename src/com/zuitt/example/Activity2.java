package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity2 {
    public static void main (String[] args){
//        ArrayList<Integer> primes = new ArrayList<>(Arrays.asList(2,3,5,7,11));
//        System.out.println("The first prime number is: " + primes.get(0));

        int [] primes = new int[] {2,3,5,7,11};
        System.out.println("The first prime number is: " + primes[0]);

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + friends);

        HashMap<String,Integer> inventory = new HashMap<>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };
        System.out.println("Our current inventory consists of : " + inventory);
    }
}
