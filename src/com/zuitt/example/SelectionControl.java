package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args){
        // Java Operators
            // Arithmetic -> +, -, *, /, %
            // Comparison -> >, <, >=, <=, ==, !=
            // Logical -> &&, ||, !
            // Assignment -> =

        // Selection Control Structure in Java
        // if else
        // Syntax :
        /*    if(condition){
                //code block
            }
            else{
                // code block
            }
        */

        int num = 36;
        if(num % 5 == 0){
            System.out.println(num + " is divisible by 5.");
        }
        else {
            System.out.println(num + " is not divisible by 5.");
        }

        //Short Circuiting
        //And operator
            //(false && ..) = false
        // Or
            //(true || ..) =true
        int x = 15;
        int y = 0;

        if(y!=0 && x/y == 0){
            System.out.println("Result: " + x/y);
        }
        else {
            System.out.println("This will only run because of short circuiting");
        }

        // Ternary operator
        int number = 24;
        Boolean result = (number > 0) ? true : false;
        System.out.println(result);

        //Switch Cases
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number from 1-4, to print four direction");

        int directionValue = numberScanner.nextInt();
        switch (directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
        }
    }
}
